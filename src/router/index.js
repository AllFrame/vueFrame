import Vue from 'vue'
import Router from 'vue-router'

import Login from '../pages/login'
import Home from '../pages/home'
import EzuikitVideoDemo from '../pages/ezuikit_video_demo'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/login',
      component: Login,
      name: '登录',
    },
    {
      path: '/home',
      component: Home,
      name: '首页',
    },
    {
      path: '/ezuikit_video_demo',
      component: EzuikitVideoDemo,
      name: '莹石播放器demo',
    },
    { //默认页面：路径为其他路径时，自动导航到首页
      path: '/*',
      component: Login,
      redirect: '/login',
      hidden: true
    },
  ]
})
