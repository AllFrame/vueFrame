// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueCookie from 'vue-cookie'
import App from './App'
import router from './router/index'
import axios from 'axios'
import {baseUrl} from './config/config'

import Mint from 'mint-ui';

import $ from 'jquery';

Vue.use(Mint);
import 'mint-ui/lib/style.css';


Vue.config.productionTip = false
Vue.use(VueCookie);
// 使用
//Vue.use(VueMaterial);


axios.defaults.baseURL = baseUrl;
axios.defaults.headers.common['Content-Type'] = 'application/json;charset=UTF-8'
axios.defaults.headers.common['token'] = VueCookie.get("token");

/* eslint-disable no-new */
//路由验证
router.beforeEach(
  (to, from, next) => {
    /*var token = localStorage.getItem('token');
    if (to.path === '/fot/view') {
      if (token == '' || token == null) {
        next({path: "/fbunlogin"});
      } else {
        next();
      }
    } else if (to.path === '/tot/trade') {
      if (token == '' || token == null) {
        next({path: "/bbunlogin"});
      } else {
        next();
      }
    } else {
      next();
    }*/
    next();
  },
  new Vue({
    el: '#app',
    router,
    components: {App},
    template: '<App/>'
  })
);

