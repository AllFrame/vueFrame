/**
 * 精度处理Util
 * @type {{toTwodecimal: calcilation.toTwodecimal}}
 */
export const calcilation = {

  //保留两位
  toTwodecimal:function (num) {
    if(num == null){
      return null;
    }
    if( (("" + num).replace(/^\d+\./, '')).length > 2 ){
      return num.toFixed(2);
    }else{
      return num;
    }
  },


  //强制保留三位
  toThreedecimal:function (num) {
    if(num == null){
      return null;
    }
    return num.toFixed(3);
  },

  //强制保留4位小数
  toFourdecimal:function (num) {
    if(num == null){
      return null;
    }
      return num.toFixed(4);
  },

  //强制保留4位小数
  toEightecimal:function (num) {
    if(num == null){
      return null;
    }
    return num.toFixed(8);
  }

}
