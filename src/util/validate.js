
export const validateEmail = email => {
	let msg = '';
	if(email == '' || email == undefined) {
		msg = "邮箱未填";
	} else if(!(/^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z\d]{2,5}$/.test(email))) {
		msg = "邮箱格式错误";
	}
	return msg;
};

export const validateMsgCode = msgCode => {
	let msg = '';
	if(msgCode == '') {
		msg = "验证码未填";
	} else if(!(/^\d{6}$/.test(msgCode))) {
		msg = "验证码为6位数字";
	}
	return msg;
};

export const validatePwd = (pwd, type = 1) => {
	let name = '';
	let msg = '';
	switch(type) {
		case 1:
			name = '密码';
			break;
		case 2:
			name = '确认密码';
			break;
		default:
			break;
	}
	if(pwd == '') {
		msg = `${name}未填`;
	} else if(!(/^[0-9A-Za-z!@#$%^&*]{6,18}$/.test(pwd))) {
		msg = `${name}由6~18位数字、字母或者特殊字符!@#$%^&*组成`;
	}
	return msg;
};

/**
 * 验证资金密码
 * @param pwd
 * @param type
 * @returns {string}
 */
export const validateMoneyPwd = (pwd, type = 1) => {
  let name = '';
  let msg = '';
  switch(type) {
    case 1:
      name = '密码';
      break;
    case 2:
      name = '确认密码';
      break;
    default:
      break;
  }
  if(pwd == '') {
    msg = `${name}未填`;
  // } else if(!(/^[0-9A-Za-z!@#$%^&*]{6,18}$/.test(pwd))) {
  //   msg = `${name}由6~18位数字、字母或者特殊字符!@#$%^&*组成`;
   }
  return msg;
};

export const confirmPwdSame = (pwd, confirmPwd) => {
	let msg = '';
	if(pwd != confirmPwd) {
		msg = "两次密码输入不一致";
	}
	return msg;
};

export const validateCheckeSta = (checked = false) => {
	let msg = '';
	if(!checked) {
		msg = "请先阅读并勾选条例";
	}
	return msg;
};

export const validateNum = (val) => {
	let msg = '';
	if(!val) {
		msg = "数量不能为空";
	}
	return msg;
};

export const validateSum = (val) => {
	let msg = '';
	if(!val) {
		msg = "金额不能为空";
	}
	return msg;
};

export const validatePrice = (val) => {
	let msg = '';
	if(!val) {
		msg = "请输入价格";
	}
	return msg;
};

export const validateLimit = (min, max) => {
	let msg = '';
	if(min) {
		if((min < 400) || (min > 500000)) {
			msg = "单笔限额应在400到500000之间";
		}
	}
	if(max) {
		if((max < 400) || (max > 500000)) {
			msg = "单笔限额应在400到500000之间";
		}
	}
	if(min && max && (min > max)) {
		msg = "单笔最大限额不能低于最小限额";
	}
	return msg;
};

/**
 * 判断是否为空
 */
export const isDataValid = (data) => {
	if(data != null && data !== '' && data !== 'undefined' && data !== 'null' && data !== undefined) {
		return true;
	}
	return false;
};
/**
 * 验证身份证合法性
 */
export const validateCard = (val) => {
	let msg = '';
	let reg = '/(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/';
	if(val == '') {
		msg = '身份证不能为空'
	} else if(!(/(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/.test(val))) {
		msg = '请输入有效的身份证';
	}
	return msg

};
/**
 * 验证有效的手机号
 */
export const validateTel = (val) => {
	let msg = '';
	if(val == '') {
		msg = '手机号不能为空';
	} else if(!(/^[1][3,4,5,7,8][0-9]{9}$/.test(val))) {
		msg = '请输入有效的手机号';
	}
	return msg

};
/**
 * 图片尺寸类型判断
 */
export const  validateImg = (img) => {
	let msg = '';
	if(!/\.(gif|jpg|jpeg|png|GIF|JPG|PNG)$/.test(img.value)) {
		msg = '图片类型必须是.gif,jpeg,jpg,png中的一种'
	} else if(img.files[0].size > 2 * 1024 * 1024) {
		msg = '上传的图片不能大于2M'
	}
	return msg;
};
