var WEAK = /^(?:\d+|[a-zA-Z]+|[!@#$%^&*]+)$/;
var MODERATE = /^(?![a-zA-z]+$)(?!\d+$)(?![!@#$%^&*]+$)[a-zA-Z\d]+$/;
var STRENGTH = /^(?![a-zA-z]+$)(?!\d+$)(?![!@#$%^&*]+$)(?![a-zA-z\d]+$)(?![a-zA-z!@#$%^&*]+$)(?![\d!@#$%^&*]+$)[a-zA-Z\d!@#$%^&*]+$/;

/**
 * 验证密码等级
 * @param val
 * @returns {number}
 */
export const validatePasswdLevel = (val) =>{
    if(WEAK.test(val)){
      return 1;
    }else if(MODERATE.test(val)){
      return 2;
    }else if(STRENGTH.test(val)){
      return 3;
    }
};
