/**
 * 配置编译环境和线上环境之间的切换
 *
 * baseUrl: 域名地址
 * imgBaseUrl: 图片所在域名地址
 *
 */
let devUrl = 'http://www.herobank.net';
let proUrl = 'http://www.herobank.net';
let imgBaseUrl = 'http://img.60community.com';
let baseUrl = '';
if (process.env.NODE_ENV == 'development') { //开发模式
  baseUrl = devUrl + '/api';
} else if (process.env.NODE_ENV == 'production') {
  baseUrl = proUrl + '/api';
}

export {
  baseUrl, imgBaseUrl
}
