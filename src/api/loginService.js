import axios from 'axios';


/**
 *  用户登录
 * @type {{login: (function(*=, *=): AxiosPromise<any>)}}
 */

export const loginService = {
  /**
   * 密码登录
   * @param loginName
   * @param pass
   * @returns {AxiosPromise<any>}
   */

  login: function (loginName, pass) {
    return axios.post('/login/in', {account: loginName, code: pass});
  }
}





