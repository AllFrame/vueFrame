# handout

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).


包设计说明：
===》src
api:接口类
assets:字体，图标，图片资源库
components:公用组件
config:api接口域名库
filter:过滤器
pages:业务页面
router:路由
style:公用scss  ==》pages:页面样式提取
util:公用类


注意事项：
1.scss命名为 中划线形式：例如：item-list
2.方法必须添加注释 例如：/** */
3.方法名 小驼峰  例如：bindLogin


//////////////////////////////////////////demo说明//////////////////////////////////////////////////////////
1.莹石播放器 视频H5/web 端接入方案
文档地址：https://open.ys7.com/doc/zh/uikit/uikit_javascript.html
demo
